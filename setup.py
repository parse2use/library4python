
from setuptools import setup, find_packages

setup(
    name='parse2use',
    version='0.1',
    packages=find_packages(exclude=['tests*']),
    license='MIT',
    description='An example python package',
    long_description=open('README.txt').read(),
    install_requires=[
        'mqtt-client',
        'paho-mqtt',
    ],
    url='https://github.com/parse2use/library4python',
    author='TAYAA Med Amine',
    author_email='tayamino@gmail.com'
)
